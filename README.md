# maodv-blackhole
- Muhsin Bayu Aji Fadhillah
- 05111850010024
- Desain Audit Jaringan

## info paper
- Judul : Performance Analysis of AODV and DSDV Routing Protocol in MANET and Modifications in AODV against Black Hole Attack
- Sumber : https://www.sciencedirect.com/science/article/pii/S1877050916002398
- Penulis : A.A.Chavana, D.S.Kurule Prof, P.U.Dere Prof.

## sekilas paper
- maodv-blackhole : modifikasi protokol aodv dengan penambahan paket verify dan final verify untuk menangani black hole attack
